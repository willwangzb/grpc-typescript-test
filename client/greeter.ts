import { GreeterClient } from "../proto/helloworld_grpc_pb";
import { credentials } from "grpc";
import { HelloReply, HelloRequest } from "../proto/helloworld_pb";
import { client } from "./utils";

const port = 3003;

export const greeter = new GreeterClient( //constructuring client
  `localhost:${port}`,
  credentials.createInsecure()
);

export const greeterClient = new GreeterClient(
  `localhost:${port}`,
  credentials.createInsecure()
);

export function sendHelloWorld(id: string) {
  return new Promise<HelloReply>((resolve, reject) => {
    const request = new HelloRequest();
    request.setName("world");

    greeterClient.sayHello(request, (err, user) => {
      console.log("entered ");

      if (err) reject(err);
      else resolve(user);
    });
  });
}
