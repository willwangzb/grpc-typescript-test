import { GreeterClient } from "../proto/helloworld_grpc_pb";
import { credentials } from "grpc";
import { HelloReply, HelloRequest } from "../proto/helloworld_pb";
import { UsersClient } from "../proto/users_grpc_pb";
import { User, UserRequest } from "../proto/users_pb";
import { client } from "./utils";

const port = 3003;

export const greeter = new UsersClient( //constructuring client
  `localhost${port}`,
  credentials.createInsecure()
);

export function mindUser(id: number) {
  return new Promise<User>((resolve, reject) => {
    const request = new UserRequest();
    request.setId(id);

    client.getUser(request, (err, user) => {
      console.log("entered ");

      if (err) reject(err);
      else resolve(user);
    });
  });
}
