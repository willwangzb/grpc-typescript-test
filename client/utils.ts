import { UsersClient } from "../proto/users_grpc_pb";
import { credentials } from "grpc";
import { GreeterClient } from "../proto/helloworld_grpc_pb";

const port = 3003;

export const client = new UsersClient(
  `localhost:${port}`,
  credentials.createInsecure()
);

export const greeterClient = new GreeterClient(
  `localhost:${port}`,
  credentials.createInsecure()
);

// export const noop = () => {};
