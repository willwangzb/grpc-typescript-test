import {
  ServerUnaryCall,
  sendUnaryData,
  ServiceError,
  ServerWritableStream,
  ServerReadableStream,
  handleUnaryCall,
} from "grpc";
import { Empty } from "google-protobuf/google/protobuf/empty_pb";

import { IUsersServer } from "../proto/users_grpc_pb";
import { User, UserRequest } from "../proto/users_pb";
import { users } from "./db";
import { IGreeterServer } from "../proto/helloworld_grpc_pb";
import { HelloRequest, HelloReply } from "../proto/helloworld_pb";

export class GreeterServer implements IGreeterServer {
  sayHello(
    call: ServerUnaryCall<HelloRequest>,
    callback: sendUnaryData<HelloReply>
  ) {
    const userId = call.request.getName();
    const helloReply = new HelloReply().setMessage("hello" + userId);

    console.log(`getUser: returning ${userId}`);
    callback(null, helloReply);
  }
}
