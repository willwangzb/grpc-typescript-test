import { Server, ServerCredentials } from "grpc";
import { GreeterService } from "../proto/helloworld_grpc_pb";
import { UsersService } from "../proto/users_grpc_pb";
import { GreeterServer } from "./greeter";
import { UsersServer } from "./services";

const server = new Server();
server.addService(UsersService, new UsersServer());
server.addService(GreeterService, new GreeterServer());

const port = 3003;
const uri = `localhost:${port}`;
console.log(`Listening on ${uri}`);
server.bind(uri, ServerCredentials.createInsecure());

server.start();
